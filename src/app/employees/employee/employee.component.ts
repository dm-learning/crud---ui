import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';
import { NgForm } from '@angular/forms';
import { Employee } from '../shared/employee.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  
  constructor(private employeeService : EmployeeService,private toastrService : ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? :NgForm){
      if (form != null)
        form.reset();

      this.employeeService.selectedEmployee = {
      empNo : null,
      firstName : '',
      lastName : '',
      designation : '',
      joiningDate : '',
      salary : null,
    }
  }

  onSubmit(form : NgForm){
    if(form.valid) {
    
        if(this.employeeService.selectedEmployee.empNo == null)
        {
          this.employeeService.saveEmployee(form.value).subscribe(response => {
            console.log(response);
            this.resetForm();
            this.toastrService.success('Record added succesfully', 'Employee Register');
            this.employeeService.getEmployeeList();
          })
        }
        else
        {
            this.employeeService.updateEmployee(this.employeeService.selectedEmployee).subscribe(response => {
            this.resetForm();
            this.toastrService.info('Record updated succesfully', 'Employee Register');
            this.employeeService.getEmployeeList();
          })
        }
    }
    else{
      console.log("Invalid form");
    }
  }
}
