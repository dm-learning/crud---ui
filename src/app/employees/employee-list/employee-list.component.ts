import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/employees/shared/employee.service';
import { Employee } from '../shared/employee.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  constructor(private employeeService : EmployeeService,private toastrService : ToastrService) { }

  ngOnInit() {
      this.employeeService.getEmployeeList();
  }

  showSelectedEmployee(employee : Employee){
    this.employeeService.selectedEmployee = Object.assign({},employee);
  }

  deleteEmployee(id : number){
    if(confirm('Are you sure you want to delete this employee ?') == true) {
      this.employeeService.deleteEmployee(id).subscribe(response => {
        this.employeeService.getEmployeeList();
        this.toastrService.warning("Record deleted successfully","Employee Register");
      })
    }
  }

}
