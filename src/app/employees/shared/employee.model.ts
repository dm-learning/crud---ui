export class Employee {
    empNo : number;
    firstName : string;
    lastName : string;
    designation : string;
    joiningDate : string;
    salary : number;
}
