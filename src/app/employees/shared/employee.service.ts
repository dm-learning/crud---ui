import { Injectable } from '@angular/core';
import { Employee } from './employee.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  selectedEmployee : Employee;
  employeeList : Employee[];
  
  constructor(private http : HttpClient) { }

  saveEmployee(employee : Employee){
    return this.http.post('http://localhost:8080/employee/add',employee);
  }

  getEmployeeList(){
    return this.http.get('http://localhost:8080/employee/all').subscribe((response : Employee[]) => {
      this.employeeList = response;
    })
  }

  updateEmployee(employee : Employee){
    return this.http.put('http://localhost:8080/employee/update',employee);
  }
  
  deleteEmployee(id : number){
    return this.http.delete('http://localhost:8080/employee/delete/' + id);
  }

}
